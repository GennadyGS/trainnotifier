﻿[string]$stationId = "9614928"
[string]$apiKey = "fedb3a60-6328-4ceb-a994-42453584a1cd"
[string]$lang = "ru"
[string]$currentDate = Get-Date -format "yyyy-MM-dd"
[string]$urlTemplate = "https://api.rasp.yandex.net/v1.0/schedule/?apikey=$apiKey&format=xml&station=s$stationId&lang=$lang&transport_types=train&date=$currentDate&event={0}"
[string]$outputDir = "Output"
[string]$xmlFileExt = ".xml"
[string]$outlookRootFolderName = "Gennadii_Saltyshchak@epam.com"
[string]$outlookCalendarName = "Trains"
[int]$outlooEventDurationMinutes = 1
[string]$outlookEventLocation="ст. Киев-Пассажирский"
[int]$outlookEventSensitivity = 2 # olPrivate
[int]$reminderMinutes = 5

Add-Type -TypeDefinition @"
   public enum EventType
   {
      arrival,
      departure
   }
"@

[bool[]] $trainNumberParityByEventType = $false, $true

[int[]] $reserveDurationMinutesByEventType = -1, 2

[string[]] $eventTypeVerbByEventType = "прибывает", "отправляется"

[string[]] $outlookCategoryByEventType = "Train Arrival", "Train Departure"

Function Get-EventData {
    Param(
        [EventType]$eventType
    )
    $url = $urlTemplate -f $eventType
    [Microsoft.PowerShell.Commands.WebResponseObject]$webResponse = Invoke-WebRequest $url
    [IO.Stream]$contentStream = $webResponse.RawContentStream
    $contentStream.Position = 0
    [IO.StreamReader]$streamReader = new-object IO.StreamReader($contentStream)
    $streamReader.ReadToEnd() | Out-File (Get-OutputFileName $eventType) -Encoding utf8
}

Function Create-EventNotifications {
    Param(
        [EventType]$eventType
    )
    Write-Host "$eventType :"
    [xml] $eventData = Get-Content (Get-OutputFileName $eventType)
    $eventData.response.schedule | % {
         $_.thread.number -match "\d+" | Out-Null; 
        [int]$number = [int]$matches[0]
        [bool]$numberParity = ($number % 2 -eq 0)
        if ($numberParity -ne $trainNumberParityByEventType[$eventType]) {
            return
        }
        [string]$eventTimeStr =  Get-ScheduleEventTime -schedule $_ -eventType $eventType
        if (!($eventTimeStr)) {
            return
        }
        [dateTime]$eventTime = $eventTimeStr
        [string]$outlookEventSubject = "Поезд $number ($($_.thread.title)) $($eventTypeVerbByEventType[$eventType]) в $($eventTime.TimeOfDay)"
        Write-Host $outlookEventSubject
        [string]$outlookEventBody = "https://rasp.yandex.ua/thread/$($_.thread.uid)",
            "Прибытие: $($_.arrival)",
            "Отправление: $($_.departure)",
            "Расписание действует: $($_.days)",
            "Кроме дней: $($_.except_days)" -join "`n"
        New-OutlookCalendarMeeting -CalendarName $outlookCalendarName `
            -Subject $outlookEventSubject `
            -Body $outlookEventBody `
            -Location $outlookEventLocation `
            -MeetingStart $eventTime.AddMinutes($reserveDurationMinutesByEventType[$eventType]) `
            -MeetingDuration $outlooEventDurationMinutes `
            -Sensitivity $outlookEventSensitivity `
            -Categories $outlookCategoryByEventType[$eventType] `
            -Reminder $reminderMinutes `
            -CheckDuplicates `
            -RootFolderName $outlookRootFolderName
    }
}

Function Get-OutputFileName {
    Param (
        [EventType] $eventType    
    )
    return Join-Path $outputDir $eventType$xmlFileExt
}

Function Get-ScheduleEventTime {
    Param(
        [object]$schedule,
        [EventType]$eventType
    )
    If ($eventType -eq [EventType]::arrival) {
        return $schedule.arrival
    }
    else {
        return $schedule.departure
    }
}

Push-Location (Split-Path $MyInvocation.MyCommand.Path -Parent)
Import-Module .\Modules\OutlookTools\OutlookTools.psm1
If (!(Test-Path $outputDir)) {
    New-Item  -Type Directory $outputDir | Out-Null
}
ForEach ($eventType in [EventType]::arrival, [EventType]::departure) {
    Get-EventData $eventType
    Create-EventNotifications $eventType
}
Pop-Location
